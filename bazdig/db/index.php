<title>bazdig database settings</title>
<link rel="stylesheet" type="text/css" href="../bazdig.css" />
<div id="nav"><a href="../console/" accesskey="c" title="(c)" class="button">console</a><a href="../history/" accesskey="h" title="(h)">history</a></div>
<form id="settings" method='get' action='./set/' >
<label>Type <input type='text' name='dbt' /></label>
<label>Name <input type='text' name='dbn' /></label>
<label>Host <input type='text' name='dbh' /></label>
<label>User <input type='text' name='dbu' /></label>
<label>Password <input type='password' name='dbp' /></label>
<input type='submit' value='Save' />
</form>
